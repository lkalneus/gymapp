function parseURLParams() {
	var url = location.search;
    var queryStart = url.indexOf("?") + 1,
        queryEnd   = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") {
        return;
    }

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=");
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) {
            parms[n] = [];
        }

        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}
var db = {
	ex : [
		[
			{
			name : "Barbell Bench Press", // Жим лежа, со штангой
			reps : 5, // Повторы
			sets : 5, // Подходы
			type : ["Chest","Shoulders", "Triceps"],
			grip : "Medium" // широкий хват ? wide
			},
			{
			name : "Barbell Incline Bench Press", // Жим на наклонной скамье
			reps : 8,
			sets : 3,
			type : ["Chest","Shoulders", "Triceps"]
			},
			{
			name : "Dumbbell Flyes", // Разводка с гантелями
			reps : 12,
			sets : 2,
			type : ["Chest"]
			},
			{
			name : "Barbell Curl", // Бицепс со штангой
			reps : 8,
			sets : 3,
			type : ["Biceps"]
			},
			{
			name : "Hammer Curls", // Молотки
			reps : 10,
			sets : 3,
			type : ["Biceps"]
			},
			{
			name : "Standing Palms-Up Barbell Behind The Back Wrist Curl", // Предплечье, стоя со штангой
			reps : 20,
			sets : 3,
			type : ["Forearms"]
			}
		],
		[
			{
			name : "Pull-Up", // Подтягивания
			reps : 10, // Повторы
			sets : 5, // Подходы
			type : ["Lats","Biceps", "Middle Back"],
			},
			{
			name : "One-Arm Dumbbell Row", // Тяга гантели
			reps : 10,
			sets : 3,
			type : ["Middle Back"]
			},
			{
			name : "Leverage Iso Row", // Тяга в хаммере
			reps : 10,
			sets : 3,
			type : ["Lats","Biceps","Middle Back"]
			},
			{
			name : "Close-Grip Barbell Bench Press", // Узкий жим
			reps : 10,
			sets : 3,
			type : ["Triceps","Chest","Shoulders"]
			},
			{
			name : "Lying Close-Grip Barbell Triceps Extension Behind The Head", // Французкий жим
			reps : 10,
			sets : 3,
			type : ["Triceps"]
			},
			{
			name : "Triceps Pushdown", // Разгибание в блоке
			reps : 10,
			sets : 3,
			type : ["Triceps"]
			},
			{
			name : "Decline Oblique Crunch", // Подъем корпуса с поворотом
			reps : 30,
			sets : 3,
			type : ["Abdominals"]
			},
			{
			name : "Hanging Leg Raise", // Поднятие ног
			reps : 30,
			sets : 3,
			type : ["Abdominals"]
			}
		],
		[
			{
			name : "Overhead Press", // Жим, сидя из-за головы
			reps : 6,
			sets : 4,
			type : ["Shoulders","Triceps"]
			},
			{
			name : "Upright Barbell Row", // Тяга штанги к подбородку
			reps : 10,
			sets : 3,
			type : ["Shoulders","Traps"]
			},
			{
			name : "Alternating Deltoid Raise", // Подъем гантелей через стороны
			reps : 12,
			sets : 3,
			type : ["Shoulders"]
			},
			{
			name : "Smith Machine Squat", // Приседания в Смитта
			reps : 10,
			sets : 4,
			type : ["Quadriceps","Calves","Glutes","Hamstrings","Lower Back"]
			},
			{
			name : "Dumbbell Rear Lunge", // Выпады назад
			reps : 12,
			sets : 3,
			type : ["Quadriceps","Calves","Glutes","Hamstrings"]
			},
			{
			name : "Lying Machine Squat", // Жим ногами
			reps : 12,
			sets : 3,
			type : ["Quadriceps","Calves","Glutes","Hamstrings"]
			},
			{
			name : "Decline Oblique Crunch", // Подъем корпуса с поворотом
			reps : 30,
			sets : 3,
			type : ["Abdominals"]
			},
			{
			name : "Hanging Leg Raise", // Поднятие ног
			reps : 30,
			sets : 3,
			type : ["Abdominals"]
			}
		],
		[
			{
			name : "Close Grip Reverse Lat Pull Down", // Тяга к себе узким обратным хватом
			reps : 10,
			sets : 3,
			type : ["Lats","Biceps","Middle Back","Shoulders"]
			},
			{
			name : "Lying Dumbbell Triceps Extension", // Французский жим с гантелями
			reps : 10,
			sets : 3,
			type : ["Triceps","Chest","Shoulders"]
			},
			{
			name : "Preacher Curl", // Бицепс на Скотта
			reps : 10,
			sets : 3,
			type : ["Biceps"]
			},
			{
			name : "Dips", // Отжимание на брусьях
			reps : 15,
			sets : 3,
			type : ["Triceps"]
			},
			{
			name : "Reverse Barbell Curl", // Бицепс со штангой обратным хватом
			reps : 15,
			sets : 3,
			type : ["Biceps","Forearms"]
			},
			{
			name : "Reverse Grip Triceps Pushdown", // Разгибание в блоке
			reps : 10,
			sets : 3,
			type : ["Triceps"]
			},
			{
			name : "Decline Oblique Crunch", // Подъем корпуса с поворотом
			reps : 30,
			sets : 3,
			type : ["Abdominals"]
			},
			{
			name : "Hanging Leg Raise", // Поднятие ног
			reps : 30,
			sets : 3,
			type : ["Abdominals"]
			}
		]
	]
}
var clock = {
	obj : {},
	init : function() {
			this.obj = document.getElementById('clock');
			var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
			var pad = function(x) {
				return x < 10 ? '0'+x : x;
			};
			
			var ticktock = function() {
				var d = new Date();
				
				var h = pad( d.getHours() );
				var m = pad( d.getMinutes() );
				var s = pad( d.getSeconds() );
				var t = days[d.getDay()];
				var current_time = [h,m,s].join(':');
				
				clock.obj.innerHTML = t + " - " + current_time;
				
			};
			
			ticktock();
			
			this.timer = setInterval(ticktock, 1000);
	}
}
var weight = {
	data : [],
// Data struct: localStorage.weight = [{d:new Date(),v:70},...]
	saveData : function(v) {
		if (v == "") return false; //todo : popup with error
		var _t = {d:new Date(),v:v};
		this.data.push(_t);
		localStorage.weight = JSON.stringify(this.data);
		var b = document.getElementById('btn');
		var rw = document.getElementById('weightInputRow');
		rw.style.cssText = "display:none";
		b.className = "btn btn-positive btn-block";
		b.innerHTML = "Saved!"
		return true;
	},
	loadData : function() {
		if (localStorage.hasOwnProperty('weight')) {
			this.data = JSON.parse(localStorage.weight);
		} 
		else {
			this.data = [];
		}
	},
	mean : function() {
		if (this.data!=[]) {
			var i = 0;
			var s = 0;
			for (i in this.data) {
				var it = this.data[i];
				s += parseFloat(it.v);
			}
			return s/(parseFloat(i)+1);
		}
	},
	updateList : function() {
		if (this.data != []) {
			var _h = "";
			for (i in this.data) {
				if ((i==0)&&(weight.data.length>0)) _h += '<li class="table-view-cell"><b>Mean:</b> '+this.mean().toFixed(1)+' kg</li>';
				var it = this.data[i];
				_h += '<li class="table-view-cell">'+new Date(weight.data[0].d).toDateString()+': '+it.v.toString()+' kg</li>';
			}
			this.ctn.innerHTML = _h;
		}
	},
	init : function() {
		this.obj = document.getElementById("weight");
		this.ctn = document.getElementById("weight-list");
		var p = parseURLParams();if (p!=undefined) { if (p.hasOwnProperty("weight")); this.loadData(); this.saveData(p.weight); }
		this.loadData();
		this.updateList();
	}
}
function init() {
	var _h = "";
		
	for (i in db.ex) {
		var _i = parseFloat(i) + 1;
		_h += '<li class="table-view-divider">Day #'+_i.toString()+'</li>'
		for (j in db.ex[i]) {
			_h += '<li class="table-view-cell">'+db.ex[i][j].name+'<span class="badge badge-primary">'+db.ex[i][j].reps+'/'+db.ex[i][j].sets+'</span></li>'
		}	
	}
	document.getElementById("extable").innerHTML = _h;
	clock.init();
}
